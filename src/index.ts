import { Command, flags } from '@oclif/command'
const express = require('express');
const cors = require('cors');
import * as https from 'https';
import * as http from 'http';
import * as fs from 'fs';
import * as os from 'os';
const pdf = require('html-pdf');
import * as moment from 'moment';
const ptp = require('pdf-to-printer');

class Printer extends Command {
    static description = 'Simple http service to print html content as a document in a system printer'

    static flags = {
        version: flags.version({ char: 'v' }),
        help: flags.help({ char: 'h' }),
        port: flags.integer({
            char: 'p',
            description: 'Port in wich the service will listen for requests, 2433 by default'
        }),
        key: flags.string({
            char: 'k',
            description: 'Path to the key file to enable https protocol (must be provided along with cert file)'
        }),
        cert: flags.string({
            char: 'c',
            description: 'Path to the cert file to enable https protocol (must be provided along with key file)'
        }),
        format: flags.enum({
            char: 'f',
            description: 'Document format that will be printed',
            options: ['A3', 'A4', 'A5', 'Legal', 'Letter', 'Tabloid', 'Kiosk-Receipt']
        }),
        width: flags.string({
            description:
                'Width of the document. Only valid when format is not defined and height is also defined. ' +
                'Unit must be specified, e.g. 80mm, 30px, etc'
        }),
        height: flags.string({
            description:
                'Height of the document. Only valid when format is not defined and width is also defined. ' +
                'Unit must be specified, e.g. 80mm, 30px, etc'
        }),
    }

    generateFile(html: string) {
        const { flags } = this.parse(Printer);
        let printOptions: any;

        if (flags.format) {
            if (flags.format === 'Kiosk-Receipt') {
                printOptions = { width: '80mm', height: '276mm' };
            }
            else {
                printOptions = { format: flags.format };
            }
        }
        else if (flags.width && flags.height) {
            printOptions = { width: flags.width, height: flags.height };
        }
        else {
            printOptions = { format: 'Letter' };
        }

        return new Promise((resolve, reject) => {
            pdf.create(html, printOptions)
                .toFile(
                    `./.temp/${moment().format('YYYY-MM-DDTHH.mm.ss.SSS')}.pdf`,
                    (err: any, res: any) => {
                        if (err) {
                            console.error(err)
                            return reject(err);
                        }

                        resolve(res.filename);
                    }
                );
        });
    }

    printDocument(filename: string) {
        return ptp.print(filename);
    }

    async run() {
        this.startServer();
    }

    startServer() {
        const { flags } = this.parse(Printer)
        let app = express();
        app.use(express.json());
        const isHttpsEnabled = flags.key && flags.cert;
        const port = flags.port || 2433;
        let server: any;

        app.use(cors({
            origin: '*'
        }));

        app.get('/rut', (request: any, response: any) => {
            response.json({ status: 'ok', message: 'I\'m here' });
        });

        app.post('/print', async (request: any, response: any) => {
            const html = request.body.document;
            console.log(`Printing requested`);

            try {
                const filename = await this.generateFile(html) as string;
                console.log(`Generated file: ${filename}`);
                await this.printDocument(filename);
                console.log('Printed document');
            }
            catch {
                console.error('Printing failed');
            }

            response.status(200);
            response.json({ status: 'ok', message: 'Document printed' });
        });

        if (isHttpsEnabled) {
            server = https.createServer(
                {
                    key: fs.readFileSync(flags.key as string),
                    cert: fs.readFileSync(flags.cert as string)
                },
                app
            )
        }
        else {
            server = http.createServer(app);
        }

        server.listen(port, () => {
            const format = flags.format ||
                (flags.width && flags.height ? `${flags.width}x${flags.height}` : 'undefined');

            const protocol = isHttpsEnabled ? 'https' : 'http';

            console.log(
                `Listening at ${protocol}://${os.hostname().toLowerCase()}:${port}/rut (format: ${format})`
            );
        });
    }
}

export = Printer
