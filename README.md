@rafaolivas/printer
===================

Simple http service to print html content as a document in a system printer

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/@rafaolivas/printer.svg)](https://npmjs.org/package/@rafaolivas/printer)
[![Downloads/week](https://img.shields.io/npm/dw/@rafaolivas/printer.svg)](https://npmjs.org/package/@rafaolivas/printer)
[![License](https://img.shields.io/npm/l/@rafaolivas/printer.svg)](https://gitlab.com/rafaolivas19/printer-service/-/tree/6-add-options-to-print-in-kiosk/package.json)

* [Installation](#installation)
* [Usage](#usage)
* [Options](#options)

# Installation

``` elm
npm install -g @rafaolivas/printer
```

# Usage

``` elm
$ printer [options]
Listening at http://computer:2433/rut (format: undefined)
```

then do a POST request to `http://computer:2433/print` with body

``` json
{
    "document": "<h1>Hello world!</h1>"
}
```

This will print a document to your default printer.

# Options

``` elm
-c, --cert=cert                                             Path to the cert file to enable https protocol (must be provided along with key file)
-f, --format=(A3|A4|A5|Legal|Letter|Tabloid|Kiosk-Receipt)  Document format that will be printed
-h, --help                                                  show CLI help
-k, --key=key                                               Path to the key file to enable https protocol (must be provided along with cert file)
-p, --port=port                                             Port in wich the service will listen for requests, 2433 by default
-v, --version                                               show CLI version
--height=height                                             Height of the document. Only valid when format is not defined and width is also defined. Unit must be specified, e.g. 80mm, 30px, etc
--width=width                                               Width of the document. Only valid when format is not defined and height is also defined. Unit must be specified, e.g. 80mm, 30px, etc
```

# Known problems

- If you configure this service to run as a Scheduled Task on Windows you should also specify that the task will run from your `$HOME` directory, otherwise there may be permission problems.